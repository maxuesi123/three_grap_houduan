export default [
  //工作台
  {
    exact: true,
    name: '工作台',
    icon: 'home',
    path: '/',
    component: '@/pages/index',
  },
  //文章管理
  {
    name: '文章管理',
    icon: 'form',
    path: '/article',
    routes: [
      //所有文章
      {
        name: '所有文章',
        path: '/article/allaricle',
        component: '@/pages/article/allarticle',
      },
      //分类文章
      {
        name: '分类文章',
        path: '/article/tagsarticle',
        component: '@/pages/article/tagsarticle',
      },
      //标签文章
      {
        name: '标签文章',
        path: '/article/typearicle',
        component: '@/pages/article/typearicle',
      },
    ],
  },
  //页面管理
  {
    exact: true,
    name: '页面管理',
    icon: 'snippets',
    path: '/pagemanagement',
    component: '@/pages/pagemanagement',
  },

  //知识小册
  {
    exact: true,
    name: '知识小册',
    icon: 'copy',
    path: '/knowledge',
    component: '@/pages/knowledge',
  },
  //海报管理
  {
    exact: true,
    name: '海报管理',
    icon: 'star',
    path: '/posters',
    component: '@/pages/posters',
  },
  //评论管理
  {
    exact: true,
    name: '评论管理',
    icon: 'message',
    path: '/comment',
    component: '@/pages/comment',
  },
  //邮件管理
  {
    exact: true,
    name: '邮件管理',
    icon: 'mail',
    path: '/mail',
    component: '@/pages/mail',
  },
  //文件管理
  {
    exact: true,
    name: '文件管理',
    icon: 'profile',
    path: '/file',
    component: '@/pages/file',
  },
  //搜素记录
  {
    exact: true,
    name: '搜素记录',
    icon: 'key',
    path: '/search',
    component: '@/pages/search',
  },
  //访问统计
  {
    exact: true,
    name: '访问统计',
    icon: 'project',
    path: '/access',
    component: '@/pages/access',
  },
  //用户管理
  {
    exact: true,
    name: '用户管理',
    icon: 'user',
    path: '/usermanagement',
    component: '@/pages/usermanagement',
  },
  //系统管理
  {
    exact: true,
    name: '系统设置',
    icon: 'setting',
    path: '/system',
    component: '@/pages/system',
  },
  //详情
  {
    exact: true,
    name: '页面管理',
    path: '/detail',
    component: '@/pages/detail',
    // // 不展示顶栏
    // headerRender: false,
    // // 不展示页脚
    // footerRender: false,
    // menuRender: false,
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  //登录
  {
    exact: true,
    name: '登录',
    path: '/login',
    component: '@/pages/login',
    icon: 'testicon',
    // 更多功能查看
    // https://beta-pro.ant.design/docs/advanced-menu
    // ---
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  //注册
  {
    exact: true,
    path: '/registered',
    name: '注册',
    component: '@/pages/registered',
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  //个人中心
  {
    path: '/geprepopersonal',
    exact: true,
    component: '@/pages/geprepopersonal',
  },
  //工作台  最新文章的详情
  {
    path: '/editor/:id',
    exact: true,
    component: '@/pages/index/editor',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
  },
  //工作台 最新评论 文章
  {
    path: '/latestActive',
    name: '后台管理',
    exact: true,
    component: '@/pages/index/latestActive',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
  //新建
  //工作台 最新评论 文章
  {
    path: '/amEditor',
    name: '新建文章',
    exact: true,
    component: '@/component/headNew/amEditor',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
];
