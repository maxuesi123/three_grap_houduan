import routes from './routes';
import { defineConfig } from 'umi';
import proxy from './proxy';
const { REACT_APP_ENV } = process.env;
export default defineConfig({
  routes: routes,
  layout: {
    name: '管理后台',
    logo: 'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
  },
  //@ts-ignore
  proxy: proxy[REACT_APP_ENV || 'dev'],
  dva: {
    immer: true,
    hmr: false,
  },
  // proxy:{
  //   '/api':{
  //     target:"https://creationapi.shbwyz.com/",
  //     changeOrigin:true,
  //     pathRewrite:{
  //       '^/api':''
  //     }
  //   }
  // }
});
