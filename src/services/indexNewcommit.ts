//@ts-ignore
import { request } from 'umi';
const res =
  JSON.parse(localStorage.getItem('res')!) &&
  JSON.parse(localStorage.getItem('res')!);
// 点击通过
export const onPass = (id: string, data: boolean) => {
  return request(`/api/comment/${id}`, {
    method: 'PATCH',
    data: {
      pass: data,
    },
    headers: {
      authorization: `Bearer ${res.token}`,
    },
    skipErrorHandler: true,
  });
};
//工作台 最新评论  回复
export const onOptions = () => {
  return request(`/api/comment`, {
    method: 'OPTIONS',
    headers: {
      authorization: `Bearer ${res.token}`,
    },
    skipErrorHandler: true,
  });
};

//工作台 最新评论  删除
export const newActiveDelete = (id: string) => {
  return request(`/api/comment/${id}`, {
    method: 'DELETE',
    skipErrorHandler: true,
  });
};
