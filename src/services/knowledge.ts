import { request } from 'umi';
//知识小册
interface knowledge {
  // page: number;
  pageSize: number;
  page: number;
}
interface file1 {
  page: number;
  pageSize: number;
}
//知识小册
export const knowledgeList = ({ page, pageSize }: knowledge) => {
  return request('/api/knowledge', {
    method: 'GET',
    params: {
      page,
      pageSize,
    },
  });
};
export const knowledgeList1 = (params: file1) => {
  return request('/api/file', {
    method: 'GET',
    params,
  });
};
// export const knowledgeList1 = ({page,pageSize}:file1) => {
//   return request('/api/file', {
//     method: 'GET',
//     params:{
//              page,
//              pageSize
//           }
// })
// }
//分页
export const pages = ({ page, pageSize }: file1) => {
  return request('/api/file', {
    method: 'GET',
    params: {
      page,
      pageSize,
    },
  });
};

export const Pushupload = (body) => {
  request('/api/file/upload?unique=0', {
    method: 'POST',
    data: body,
  });
};
