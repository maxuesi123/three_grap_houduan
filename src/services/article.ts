import { request } from 'umi';

export const articleList = (params: { page: number; pageSize: number }) => {
  return request('/api/article', {
    method: 'GET',
    params,
  });
};
export const deleteArticleList = (id: any) =>
  request(`/api/article/${id}`, {
    method: 'DELETE',
  });
export const DetailList = (id: any) => {
  return request(`/api/article/${id}`, {
    method: 'GET',
  });
};
