//@ts-ignore
import { request } from 'umi';
interface logintype {
  name: string;

  password: any;
}
interface files {
  page: number;
  pageSize: number;
}
interface registertype {
  name: string;
  password: number | string;
  confirm: number | string;
}

interface newcommit {
  page: number;
  pageSize: number;
}
//登录页接口
export const login = (data: logintype) => {
  return request('/api/auth/login', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
};

//文件管理数据
export const fileList = ({ page, pageSize }: files) => {
  return request('/api/file', {
    method: 'GET',
    params: {
      page,
      pageSize,
    },
  });
};
// 搜索数据
export const searchList = (params: files) => {
  return request('/api/search', {
    method: 'GET',
    params,
  });
};
//注册页
export const register = (data: registertype) => {
  return request('/api/user/register', {
    method: 'post',
    data,
    skipErrorHandler: true,
  });
};
//最新文章
export const indexZhuiactive = ({ page, pageSize }: newcommit) => {
  return request('/api/article', {
    method: 'get',
    params: {
      page,
      pageSize,
    },
    skipErrorHandler: true,
  });
};
//最新评论
export const indexNewactive = ({ page, pageSize }: newcommit) => {
  return request('/api/comment', {
    method: 'get',
    params: {
      page,
      pageSize,
    },
    skipErrorHandler: true,
  });
};

//用户管理
export const UserManagement = ({ page, pageSize }: newcommit) => {
  return request('/api/user', {
    method: 'get',
    params: {
      page,
      pageSize,
    },
    skipErrorHandler: true,
  });
};

//工作台  最新文章点击事件  跳到Leetcode
export const newActiveLeeCode = () => {
  return request('/api/article', {
    method: 'get',
    skipErrorHandler: true,
  });
};

//访问统计
export const access = ({ page, pageSize }: newcommit) => {
  return request('/api/view', {
    method: 'get',
    params: {
      page,
      pageSize,
    },
    skipErrorHandler: true,
  });
};
