import { request } from 'umi';

export const pageList = (params: { page: number; pageSize: number }) => {
  return request('/api/page', {
    method: 'GET',
    params,
  });
};
