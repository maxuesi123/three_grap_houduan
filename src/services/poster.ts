import { request } from 'umi';

export const Pushuploads = (body) => {
  request('/api/file/upload?unique=0', {
    method: 'POST',
    data: body,
  });
};
