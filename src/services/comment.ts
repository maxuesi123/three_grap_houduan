import { request } from 'umi';
interface Params {
  id: string;
  parentId: null;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content: null;
  html: null;
  toc: null;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
//评论管理页面
export const commentList = () =>
  request('/api/comment', {
    method: 'GET',
    skipErrorHandler: true,
  });
