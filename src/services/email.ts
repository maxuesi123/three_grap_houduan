import { request } from 'umi';
//邮件页面管理数据
export const emailList = () => {
  return request('/api/smtp', {
    method: 'GET',
    skipErrorHandler: true,
  });
};
