import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { indexZhuiactive } from '@/services/user';
export interface AritcleModelState {
  title: string;
  list: any[];
  total: number;
}
export interface AritcleModelType {
  namespace: string;
  state: AritcleModelState;
  reducers: {
    changeTitle: Reducer<AritcleModelState>;
    changeList: Reducer<AritcleModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<IndexModelState>;
  };
  effects: {
    getarticle: Effect;
  };
}
const article: AritcleModelType = {
  namespace: 'article',
  state: {
    title: '1909b',
    list: [],
    total: 0,
  },
  // save(state, action) {
  //     return {
  //       ...state,
  //       ...action.payload,
  //     };
  // },
  effects: {
    //相当于vuex里面的action，异步
    *getarticle({ payload }, { call, put }) {
      console.log(payload);
      // // call 方法来发请求的（请求的函数，请求的参数）
      let res = yield call(indexZhuiactive, payload);
      console.log(res, 'res****************');
      yield put({
        type: 'changeList',
        payload: res.data,
      });
    },
  },
  reducers: {
    //相当于vuex里面的mutation
    changeTitle(state, action) {
      console.log(state, action, 'changetitle');
      return {
        ...state,
        ...action.payload,
      };
    },
    changeList(state, action) {
      //修改状态
      // console.log(state,action,'changelist');
      return {
        ...state,
        ...action.payload,
        list: action.payload[0],
        total: action.payload[1],
      };
    },
    // 启用 immer 之后
    // save(state, action) {
    //   state.name = action.payload;
    // },
  },
};
export default article;
