// import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
// import { onPass } from '@/services/indexNewcommit';
// export interface AritcleModelState {
//   title: string;
//   list: any[];
//   total: number;
// }
// export interface AritcleModelType {
//   namespace: string;
//   state: AritcleModelState;
// //   reducers: {
// //     changeTitle: Reducer<AritcleModelState>;
// //     changeList: Reducer<AritcleModelState>;
// //     // 启用 immer 之后
// //     // save: ImmerReducer<IndexModelState>;
// //   };
//   effects: {
//     changePass: Effect;
//   };
// }
// const index: AritcleModelType = {
//   namespace: 'article',
//   state: {
//     title: '1909b',
//     list: [],
//     total: 0,
//   },

//   effects: {
//     //相当于vuex里面的action，异步
//     *changePass({ payload }: any, { call, put }: any) {
//         // console.log(payload,'payload')
//         let res = yield call(onPass, payload);
//         // 发送请求
//         yield put({
//           type: 'setChangePass',
//           payload: res.data,
//         });
//       },
//   },
// //   reducers: {
// //     //相当于vuex里面的mutation
// //     // changeTitle(state, action) {
// //     //   console.log(state, action, 'changetitle');
// //     //   return {
// //     //     ...state,
// //     //     ...action.payload,
// //     //   };
// //     // },
// //     // changeList(state, action) {
// //     //   //修改状态
// //     //   // console.log(state,action,'changelist');
// //     //   return {
// //     //     ...state,
// //     //     ...action.payload,
// //     //     list: action.payload[0],
// //     //     total: action.payload[1],
// //     //   };
// //     // },
// //     // 启用 immer 之后
// //     // save(state, action) {
// //     //   state.name = action.payload;
// //     // },
// //   },
// };
// export default index;
