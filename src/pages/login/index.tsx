import React, { FC } from 'react';
import { useRequest, useHistory } from 'umi';
import { Form, Input, Button, message } from 'antd';
import { login } from '@/services/user';
import styles from './index.less';
import loginimg from './img/loginimg.png';
import { useState } from 'react';
const App: FC = () => {
  const { run, loading } = useRequest(login, { manual: true });
  // const { run } = useRequest(register, {manual: true});
  const [loginflag, setflags] = useState(false);
  const history = useHistory();

  const onFinish = async (values: any) => {
    //点击登录按钮调登录   hooks  必须写到函数的顶部
    let res = await run(values);
    if (res) {
      window.localStorage.setItem('token', res.token);
      //进行修改一下
      window.localStorage.setItem('res', JSON.stringify(res));
      history.push('/');
      message.info('登录成功');
    } else {
      message.error('登录失败');
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  //去注册
  const handeresigter = () => {
    console.log(222);
    history.push('/registered');
  };

  return (
    <div className={styles.login}>
      <div className={styles.center}>
        <div className={styles.left}>
          <img src={loginimg} alt="" />
        </div>
        <div className={styles.right}>
          <div className={styles.logina}>
            <h3>系统登录</h3>
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              className={styles.loginform}
            >
              <Form.Item
                label="用户名"
                name="name"
                rules={[
                  { required: true, message: 'Please input your username!' },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="密码"
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button loading={loading} type="primary" htmlType="submit">
                  登录
                </Button>
              </Form.Item>
            </Form>
            <p>
              Or <span onClick={handeresigter}>去注册</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
