import React, { useState } from 'react';
import { Table, Button } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import './access.less';
import { useHistory } from 'umi';
import { access } from '@/services/user';
import { Badge, Space, Switch } from 'antd';
import moment from 'moment';
import { useEffect } from 'react';
import { debounce } from 'lodash';
import { ClockCircleOutlined } from '@ant-design/icons';
//用户管理数据

interface DataType {
  key: React.Key;
  name: string;
  age: string;
  address: string;
  ky: string;
  date: string;
}

const App: React.FC = () => {
  // 访问量徽标
  const [show, setShow] = useState(true);
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);
  const [page, usePage] = useState(1); //当前页 1页
  const [pageSize, usePageSize] = useState(12); //当前页一共有条
  const [useData, useUserdata] = useState([]); //获取表格里的数据
  const [userlenght, useuserlenght] = useState(0);
  // 模糊搜索
  const [useData1, useUserdata1] = useState([]);
  const [valData, valUserdata] = useState('');

  useEffect(() => {
    UserMana();
  }, []);
  const UserMana = async () => {
    const useObj = { page: page, pageSize: pageSize };
    useObj.page = page;
    useObj.pageSize = pageSize;

    const res = await access(useObj);
    if (res) {
      useUserdata(res.data[0]);
      useUserdata1(res.data[0]);
      useuserlenght(res.data[1]);
    }
  };
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };
  const columns: ColumnsType<DataType> = [
    {
      title: 'URL',
      width: '220px',
      dataIndex: 'url',
      key: 'id',
      fixed: 'left',
      render(text: any) {
        return <a key={text}>{text}</a>;
      },
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      key: 'id',
      width: '170px',
    },
    {
      title: '浏览器',
      width: '160px',
      dataIndex: 'browser',
      key: 'id',
    },
    {
      title: '内核',
      dataIndex: 'engine',
      key: 'id',
      width: '160px',
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      key: 'id',
      width: '160px',
    },
    {
      title: '设备',
      dataIndex: 'device',
      width: '160px',
      key: 'id',
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: 'id',
      width: '120px',
    },
    {
      title: '访问量',
      dataIndex: 'count',
      key: 'id',
      width: '110px',
      render: (_record) => (
        <Badge
          className="site-badge-count-109"
          count={show ? _record : 0}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '访问时间',
      dataIndex: 'updateAt',
      key: 'id',
      width: '190px',
      render: (_record) => (
        <span>{moment(_record).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: '操作',
      fixed: 'right',
      key: 'action',
      width: '150px',
      render: (_, record) => (
        <div>
          <a>删除</a>
        </div>
      ),
    },
  ];

  const hasSelected = selectedRowKeys.length > 0;

  const handpagesize = (page: number) => {
    usePage(page);
    UserMana();
  };
  console.log(useData, '111111111');

  const history = useHistory();
  // 跳转到工作台
  const work = () => {
    history.push('/');
  };

  //模糊搜索
  const handaearch = (e) => {
    valUserdata(e.target.value);
    console.log(valData, '----------valData--------------');
  };
  const handtars = () => {
    console.log(useData, useData1);
    // useData1 useData
    // 地址
    const val = useData1.filter((item, index) => {
      return item.os.includes(valData);
    });
    useUserdata(val);
    console.log(val);
  };

  return (
    <div className="big">
      {/* 跳到工作台 */}
      <div className="topone">
        <span onClick={work} style={{ color: 'grey' }}>
          工作台
        </span>{' '}
        &emsp; /<span>访问统计</span>
      </div>
      {/* 搜索 */}
      <div className="toptwo">
        <div className="first">
          IP：
          <input type="text" placeholder=" 请输入IP地址" />
        </div>
        <div className="first">
          UA：
          <input type="text" placeholder=" 请输入 User Agent" />
        </div>
        <div className="first">
          URL：
          <input type="text" placeholder=" 请输入 URL" />
        </div>
        <div className="first">
          地址：
          <input type="text" placeholder=" 请输入地址" onChange={handaearch} />
        </div>

        <br />

        <div>
          浏览器：
          <input type="text" placeholder=" 请输入浏览器" />
        </div>
        <div>
          内核：
          <input type="text" placeholder=" 请输入内核" />
        </div>
        <div>
          OS：
          <input
            type="text"
            placeholder=" 请输入操作系统"
            onChange={handaearch}
          />
        </div>
        <div>
          设备：
          <input type="text" placeholder=" 请输入设备" />
        </div>

        <div className="but">
          <button className="one" onClick={handtars}>
            搜索
          </button>
          <button className="two">重置</button>
        </div>
      </div>
      {/* 下面数据  -  分页 */}
      <div className="mainn">
        <div style={{ marginBottom: 16 }}>
          {/* <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}> */}
          <Button type="primary" disabled={!hasSelected} loading={loading}>
            {/* antd图标 */}
            <span role="img" aria-label="undo" className="anticon anticon-undo">
              <svg
                viewBox="64 64 896 896"
                focusable="false"
                data-icon="undo"
                width="1em"
                height="1em"
                fill="currentColor"
                aria-hidden="true"
              >
                <path d="M511.4 124C290.5 124.3 112 303 112 523.9c0 128 60.2 242 153.8 315.2l-37.5 48c-4.1 5.3-.3 13 6.3 12.9l167-.8c5.2 0 9-4.9 7.7-9.9L369.8 727a8 8 0 00-14.1-3L315 776.1c-10.2-8-20-16.7-29.3-26a318.64 318.64 0 01-68.6-101.7C200.4 609 192 567.1 192 523.9s8.4-85.1 25.1-124.5c16.1-38.1 39.2-72.3 68.6-101.7 29.4-29.4 63.6-52.5 101.7-68.6C426.9 212.4 468.8 204 512 204s85.1 8.4 124.5 25.1c38.1 16.1 72.3 39.2 101.7 68.6 29.4 29.4 52.5 63.6 68.6 101.7 16.7 39.4 25.1 81.3 25.1 124.5s-8.4 85.1-25.1 124.5a318.64 318.64 0 01-68.6 101.7c-7.5 7.5-15.3 14.5-23.4 21.2a7.93 7.93 0 00-1.2 11.1l39.4 50.5c2.8 3.5 7.9 4.1 11.4 1.3C854.5 760.8 912 649.1 912 523.9c0-221.1-179.4-400.2-400.6-399.9z"></path>
              </svg>
            </span>
          </Button>
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `选中 ${selectedRowKeys.length} 条` : ''}
          </span>
        </div>
        <Table
          columns={columns}
          dataSource={useData}
          scroll={{ x: 1500 }}
          rowSelection={rowSelection}
          //@ts-ignore
          pagination={{
            total: userlenght,
            //@ts-ignore
            page: page,
            pageSize: pageSize,
            onChange: handpagesize,
          }}
        />
      </div>
    </div>
  );
};

export default App;
