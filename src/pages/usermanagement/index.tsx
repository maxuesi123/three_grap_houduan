import React, { useState } from 'react';
import { Table, Button } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import './user.less';
import { useHistory } from 'umi';
import { useEffect } from 'react';
//用户管理数据
import { UserManagement } from '@/services/user';
import { Input, Select } from 'antd';
import { render } from 'react-dom';
import moment from 'moment';
import styles from './user.less';
import { debounce, includes } from 'lodash';

const { Option } = Select;

interface DataType {
  key: React.Key;
  name: string;
  age: string;
  address: string;
  ky: string;
  date: string;
  // operation:string;
}

const App: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [loading, setLoading] = useState(false);
  const [page, usePage] = useState(1); //当前页 1页
  const [pageSize, usePageSize] = useState(12); //当前页一共有条
  const [useData, useUserdata] = useState([]); //获取表格里的数据
  // 模糊搜索
  const [useData1, useUserdata1] = useState([]);
  const [valData, valUserdata] = useState('');
  //
  const [userlenght, useuserlenght] = useState(0);
  useEffect(() => {
    UserMana();
  }, []);
  const UserMana = async () => {
    const useObj = { page: page, pageSize: pageSize };
    useObj.page = page;
    useObj.pageSize = pageSize;

    const res = await UserManagement(useObj);
    if (res) {
      useUserdata(res.data[0]);
      useUserdata1(res.data[0]);
      useuserlenght(res.data[1]);
    }
  };
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };
  const columns: ColumnsType<DataType> = [
    {
      title: '账户',
      dataIndex: 'name',
      key: 'id',
      width: '230px',
      render(text: any) {
        return <span key={text}>{text}</span>;
      },
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'id',
      fixed: 'left',
    },
    {
      title: '角色',
      dataIndex: 'role',
      key: 'id',
      fixed: 'left',
      render: (text) => <div>{text === 'admin' ? '管理员' : '访客'}</div>,
    },
    {
      title: '状态',
      dataIndex: 'pass',
      key: 'id',
      fixed: 'left',
      render: (text) => (
        <div>
          <span className={styles.span_style}></span>
          {text === 'status' ? '可用' : '已锁定'}
        </div>
      ),
    },
    {
      title: '注册日期',
      dataIndex: 'createAt',
      key: 'id',
      fixed: 'left',
      render: (_record) => (
        <span>{moment(_record).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: '操作',
      fixed: 'right',
      key: 'action',
      render: (_, record) => (
        <div>
          <a>禁用 </a>
          <a>授权 </a>
        </div>
      ),
    },
  ];

  const hasSelected = selectedRowKeys.length > 0;

  const history = useHistory();
  // 跳转到工作台
  const work = () => {
    history.push('/');
  };
  const handpagesize = (page: number) => {
    usePage(page);
    UserMana();
  };
  console.log(useData, '113');
  //模糊搜索
  const handaearch = (e) => {
    valUserdata(e.target.value);
    console.log(valData, '-4--4----4--4--4-------------');
  };
  const handtar = () => {
    console.log(useData, useData1);
    // useData1 useData
    const val = useData1.filter((item, index) => {
      return item.name.includes(valData);
    });
    useUserdata(val);
    console.log(val);
  };

  return (
    <div>
      <div className="Topone">
        <span onClick={work} style={{ color: 'grey' }}>
          工作台
        </span>{' '}
        &emsp; /<span>用户管理</span>
      </div>
      <div className="Toptwo">
        <div>
          账户：
          <input
            type="text"
            placeholder=" 请输入用户账户"
            onChange={handaearch}
          />
        </div>
        <div>
          邮箱：
          <input type="text" placeholder=" 请输入账户邮箱" />
        </div>
        <div>
          角色：{' '}
          <select name="" id="" defaultValue=" ">
            <option value=" ">管理员</option>
            <option value=" ">访客</option>
          </select>
        </div>
        <div>
          状态：{' '}
          <select name="" id="" defaultValue=" ">
            <option value=" ">锁定</option>
            <option value=" ">可用</option>
          </select>
        </div>

        <div className="but">
          <button className="one" onClick={handtar}>
            搜索
          </button>
          <button className="two">重置</button>
        </div>
      </div>
      <div className="mainning">
        <div style={{ marginBottom: 16 }}>
          {/* <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}> */}
          <Button type="primary" disabled={!hasSelected} loading={loading}>
            {/* antd图标 */}
            <span role="img" aria-label="undo" className="anticon anticon-undo">
              <svg
                viewBox="64 64 896 896"
                focusable="false"
                data-icon="undo"
                width="1em"
                height="1em"
                fill="currentColor"
                aria-hidden="true"
              >
                <path d="M511.4 124C290.5 124.3 112 303 112 523.9c0 128 60.2 242 153.8 315.2l-37.5 48c-4.1 5.3-.3 13 6.3 12.9l167-.8c5.2 0 9-4.9 7.7-9.9L369.8 727a8 8 0 00-14.1-3L315 776.1c-10.2-8-20-16.7-29.3-26a318.64 318.64 0 01-68.6-101.7C200.4 609 192 567.1 192 523.9s8.4-85.1 25.1-124.5c16.1-38.1 39.2-72.3 68.6-101.7 29.4-29.4 63.6-52.5 101.7-68.6C426.9 212.4 468.8 204 512 204s85.1 8.4 124.5 25.1c38.1 16.1 72.3 39.2 101.7 68.6 29.4 29.4 52.5 63.6 68.6 101.7 16.7 39.4 25.1 81.3 25.1 124.5s-8.4 85.1-25.1 124.5a318.64 318.64 0 01-68.6 101.7c-7.5 7.5-15.3 14.5-23.4 21.2a7.93 7.93 0 00-1.2 11.1l39.4 50.5c2.8 3.5 7.9 4.1 11.4 1.3C854.5 760.8 912 649.1 912 523.9c0-221.1-179.4-400.2-400.6-399.9z"></path>
              </svg>
            </span>
          </Button>
          <span style={{ marginLeft: 8 }}>
            {hasSelected ? `选中 ${selectedRowKeys.length} 条` : ''}
          </span>
        </div>
        <Table
          columns={columns}
          dataSource={useData}
          rowSelection={rowSelection}
          //@ts-ignore
          pagination={{
            total: userlenght,
            //@ts-ignore
            page: page,
            pageSize: pageSize,
            onChange: handpagesize,
          }}
        />
      </div>
    </div>
  );
};

export default App;
