import styles from './index.less';
import FileLess from './search.module.less';
import { searchList } from '../../services/user';
import { useState, useEffect } from 'react';
import { Table } from 'antd';
import type { TableRowSelection } from 'antd/lib/table/interface';
import type { ColumnsType } from 'antd/lib/table';
import { useHistory } from 'umi';
import moment from 'moment';
import { Badge, Space, Switch } from 'antd';
interface DataType {
  key: React.Key;
  name: string;
  num: number;
  time: number;
}

// const data: DataType[] = [];
// for (let i = 0; i < 50; i++) {
//   data.push({
//     key: i,
//     name: `Edward King ${i}`,
//     num: 32,
//     time: +new Date(),
//   });
// }

const App: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [searchData, usesearchData] = useState([]);
  const [page, usePage] = useState(1); //当前页 1页
  const [pageSize, usePageSize] = useState(12); //当前页一共有条
  const [show, setShow] = useState(true);
  //搜索数据
  useEffect(() => {
    getsearch();
  }, []);
  const getsearch = async () => {
    const useObj = { page: page, pageSize: pageSize };
    useObj.page = page;
    useObj.pageSize = pageSize;
    const res = await searchList(useObj);
    console.log(res.data[0]);
    usesearchData(res.data[0]);
  };

  //表格数据
  const columns: ColumnsType<DataType> = [
    {
      title: '搜索词',
      dataIndex: 'keyword',
      key: 'id',
    },
    {
      title: '搜索量',
      dataIndex: 'count',
      key: 'id',
      render: (_record) => (
        // <span> {_record}</span>
        <Badge
          className="site-badge-count-109"
          count={show ? _record : 0}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '搜索时间',
      dataIndex: 'updateAt',
      key: 'id',
      render: (_record) => (
        <span>{moment(_record).format('YYYY-MM-DD HH:mm:ss')}</span>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: () => <a>删除</a>,
    },
  ];
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };
  return (
    <div className={FileLess.search}>
      <div className={FileLess.header}>
        <p>
          <span>工作台</span>/<span>搜索记录</span>
        </p>
      </div>
      <div className={FileLess.main}>
        <div className={FileLess.main_bom}>
          <div className={FileLess.bom_top}>
            &emsp; 类型： <input type="text" />
            &emsp; 搜索词：
            <input type="text" />
            &emsp; 搜索量：
            <input type="text" />
            <p>
              <button className={FileLess.select}>搜索</button>
              <button>重置</button>
            </p>
          </div>
          <div className={FileLess.bom_bom}>
            <Table
              rowSelection={rowSelection}
              columns={columns}
              dataSource={searchData}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
