import styles from './index.less';
import { useState, useEffect, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card, Table, Space, message, Popconfirm, Modal, Popover } from 'antd';
import * as echarts from 'echarts';
import { useHistory } from 'umi';
import { indexZhuiactive, indexNewactive } from '@/services/user';
import type { ColumnsType } from 'antd/lib/table';
import { onPass, onOptions, newActiveDelete } from '@/services/indexNewcommit';
import { ActionType } from '@ant-design/pro-table';
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}
const IndexPage = () => {
  const [data, setdata] = useState();
  const [newcommit, setnewcommit] = useState(); //最新评论
  const [page, setpage] = useState(1); //分页 当前页
  const [pageSize, setpageSize] = useState(6); //分页  当前页有几个
  const [visible, setVisible] = useState(false);
  const [huifuvisible, setHuifuvisible] = useState(false);
  const [visitors, setVisitors] = useState(); // 获取登录页  访问角色
  const [Tongflag, usenewTongflag] = useState(false);
  const [commitList, setcommitList] = useState({});
  const [navdata, setnavdata] = useState([
    { name: '文章管理', path: '/article/allaricle' },
    { name: '评论管理', path: '/comment' },
    { name: '文件管理', path: '/file' },
    { name: '用户管理', path: '/usermanagement' },
    { name: '访问管理', path: '/access' },
    { name: '系统管理', path: '/system' },
  ]);
  const [ZXactiveData, setZXactiveData] = useState();
  const history = useHistory();
  const myref = useRef();
  const res = useRef<ActionType>();
  const init = () => {
    res.current && res.current.reload();
  };
  //获取登录页的用户名
  useEffect(() => {
    if (window.localStorage.getItem('res')) {
      //@ts-ignore
      const logindataRes = JSON.parse(window.localStorage.getItem('res'));
      return setdata(logindataRes.name);
    }
  }, []);
  //获取登录页的角色
  useEffect(() => {
    if (window.localStorage.getItem('res')) {
      //@ts-ignore
      const logindataRes = JSON.parse(window.localStorage.getItem('res'));
      if (logindataRes.role === 'admin') {
        //@ts-ignore
        return setVisitors('管理员');
      } else {
        //@ts-ignore
        return setVisitors('访客');
      }
    }
  }, []);
  //echarts图标
  useEffect(() => {
    //@ts-ignore
    var myChart = echarts.init(myref.current);
    const option = {
      title: {
        text: '每周用户访问指标',
      },
      legend: {
        data: ['评论数', '访问量'],
      },
      dataset: {
        source: [['评论数', '访问量 ']],
      },
      tooltip: {
        trigger: 'axis',
        showContent: false,
      },
      //@ts-ignore
      grid: false,

      xAxis: {
        axisPointer: {
          type: 'shadow',
        },
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      },
      yAxis: [
        {
          type: 'value',
          min: 0,
          max: 400,
          interval: 100,
        },
      ],
      series: [
        {
          name: '评论数',
          data: [120, 200, 150, 90, 70, 110, 130],
          type: 'bar',
          color: '#c23531',
          barWidth: '120px',
        },
        {
          name: '访问量',
          type: 'line',
          color: '#484545',
          data: [350, 290, 340, 130, 100, 140, 160],
        },
      ],
    };
    myChart.setOption(option);
  }, []);
  //接受最新文章数据
  useEffect(() => {
    activedata();
  }, []);
  const activedata = async () => {
    let obj = { page, pageSize };
    obj.page = page;
    obj.pageSize = pageSize;
    let res = await indexZhuiactive(obj);
    setZXactiveData(res.data[0]);
  };
  //接受最新评论数据
  useEffect(() => {
    commitdata();
  }, []);
  const commitdata = async () => {
    const data = { page, pageSize };
    data.page = page;
    data.pageSize = pageSize;
    let res = await indexNewactive(data);
    setnewcommit(res.data[0]);
  };
  //导航路由
  const handNavsPath = (index: number) => {
    history.push(navdata[index].path);
  };

  //全部文章
  const handAllactive = () => {
    history.push('/article/allaricle');
  };
  //全部评论
  const handCommit = () => {
    history.push('/comment');
  };
  //全部评论的数据 表格
  const columns: ColumnsType<DataType> = [
    {
      dataIndex: 'name',
      key: 'name',
      width: '160px',
      render(_record: any) {
        return (
          <span key={_record}>
            {_record}在
            <Popover
              content={
                <div className={styles.popovers}>
                  404 | This page could not be found.
                </div>
              }
              placement="right"
              title="页面预览"
            >
              <a href="" onClick={handNewActive}>
                文章
              </a>
            </Popover>
            评论
          </span>
        );
      },
    },
    {
      dataIndex: 'content',
      key: 'content',
      fixed: 'left',
      render: (record) => {
        return (
          <Popover
            className={styles.popoverTable}
            content={record}
            title="评论详情-原始内容"
          >
            <a href="">查看内容</a>
          </Popover>
        );
      },
    },
    {
      dataIndex: 'pass',
      key: 'pass',
      fixed: 'left',
      render: (text) => (
        <div className="tablelettongguo">
          <span className="ant-badge ant-badge-status ant-badge-not-a-wrapper">
            {text === true ? (
              <p>
                <span className="span_style___lH_9g"></span>
                <span className="ant-badge-status-text">通过</span>
              </p>
            ) : (
              <p>
                {' '}
                <span className="ant-badge-status-dot ant-badge-status-gold"></span>
                <span className="ant-badge-status-text">未通过</span>
              </p>
            )}
          </span>
        </div>
      ),
    },
    {
      fixed: 'right',
      key: 'action',
      render: (_, record: any) => (
        <Space size="middle" key={record.id}>
          <a
            onClick={() => {
              //@ts-ignore
              if (logindataRes.role === 'visitor') {
                message.warn('访客无权进行该操作');
              } else {
                record.pass = true;
                console.log(record.pass);
                //@ts-ignore
                onPass(record.id, record.pass).then((res) => {
                  message.success('评论已通过');
                  console.log(res);
                  commitdata();
                  init();
                });
              }
            }}
          >
            通过{' '}
          </a>
          <a
            onClick={() => {
              //@ts-ignore
              if (logindataRes.role === 'visitor') {
                message.warn('访客无权进行该操作');
              } else {
                record.pass = false;
                console.log(record.pass);
                //@ts-ignore
                onPass(record.id, record.pass).then((res) => {
                  message.success('评论已拒绝');
                  console.log(res);
                  commitdata();
                  init();
                });
              }
            }}
          >
            拒绝{' '}
          </a>
          <a onClick={showModal}>回复 </a>
          <Popconfirm
            title="您确定要删除这个评论吗？"
            okText="Yes"
            cancelText="No"
            onConfirm={() => confirm(record.id)}
          >
            {' '}
            <a>删除 </a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  //最新评论 删除
  const confirm = (id: string) => {
    setVisible(false);
    message.warn('访客无权访问操作');
    if (logindataRes.role === 'visitor') {
      message.warning('访客无权进行该操作');
    } else {
      newActiveDelete(id).then((res) => {
        if (res.statusCode === 200) {
          message.success('删除成功');
          commitdata();
        }
      });
    }
  };
  //最新评论  文章跳转页面
  const handNewActive = () => {
    history.push('/latestActive');
  };
  //最新文章的点击事件
  const handCardNewactive = (item: any) => {
    history.push(`/editor/${item.id}`, item);
  };
  const showModal = () => {
    setHuifuvisible(true);
  };

  const hideModal = () => {
    setHuifuvisible(false);
    // //@ts-ignore
    // if(logindataRes.role==='visitor'){
    //   message.warn("访客无权进行该操作")
    // }
    // else{
    //   record.pass=true;
    //   console.log(record.pass)
    //   //@ts-ignore
    //   onOptions(record.id,record.pass).then(res=>{
    //     message.success("评论已通过")
    //     console.log(res)
    //     commitdata()
    //     init()
    //   });
    // }
  };
  return (
    <div className={styles.title}>
      <PageContainer
        className={styles.pagecontainers}
        title={'您好,' + data}
        content={`您的角色: ${visitors}`}
      ></PageContainer>
      <br />
      <div className={styles.center}>
        {/* 面板导航 echarts图表 */}
        <Card>
          <Card>面板导航</Card>
          {/* echarts图表 */}
          <Card id={styles.card}>
            {/*@ts-ignore */}
            <div id={styles.echarts} ref={myref}></div>
          </Card>
        </Card>
      </div>
      {/* 快速导航 */}
      <div className={styles.navs}>
        <Card>
          <Card> 快速导航</Card>
          <Card>
            <div className={styles.navslsit}>
              {navdata &&
                navdata.map((item, index) => {
                  return (
                    <p key={index} onClick={() => handNavsPath(index)}>
                      {item.name}
                    </p>
                  );
                })}
            </div>
          </Card>
        </Card>
      </div>
      {/* 最新文章  */}
      <div className={styles.footerActive}>
        <Card>
          <div className={styles.footerspan}>
            <span>最新文章</span>
            <span onClick={handAllactive}>全部文章</span>
          </div>
        </Card>
        <Card>
          <div className={styles.zuixinwenzhang}>
            {
              //@ts-ignore
              ZXactiveData &&
                //@ts-ignore
                ZXactiveData.map((item: any, index: number) => {
                  return (
                    <Card
                      className={styles.wenzhang}
                      key={index}
                      onClick={() => handCardNewactive(item)}
                    >
                      <img src={item.cover} alt="" />
                      <p>{item.title}</p>
                    </Card>
                  );
                })
            }
          </div>
        </Card>
      </div>
      <div className={styles.footercomment}>
        <Card>
          <div className={styles.footerspan}>
            <span>最新评论</span>
            <span onClick={handCommit}>全部评论</span>
          </div>
        </Card>
        <Card>
          <div className={styles.newcomments}>
            <Table
              columns={columns}
              dataSource={newcommit}
              pagination={false}
            />
          </div>
        </Card>
      </div>
      <Modal
        title="回复评论"
        visible={huifuvisible}
        onOk={hideModal}
        onCancel={hideModal}
        okText="确认"
        cancelText="取消"
      >
        <textarea placeholder="支持 Markdown" className="ant-input"></textarea>
      </Modal>
    </div>
  );
};
function itme(itme: any): void {
  throw new Error('Function not implemented.');
}
function newCommitdata(newCommitdata: any, arg1: string) {
  throw new Error('Function not implemented.');
}
export default IndexPage;

function logindataRes(logindataRes: any) {
  throw new Error('Function not implemented.');
}

function id(id: any) {
  throw new Error('Function not implemented.');
}
// connect((state)=>{return state})()
//
