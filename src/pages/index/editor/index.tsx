import {
  useEffect,
  useState,
  useRef,
  ReactChild,
  ReactFragment,
  ReactPortal,
} from 'react';
import { Card } from 'antd';
import styles from './index.less';
import {
  Button,
  Dropdown,
  Menu,
  Space,
  Divider,
  message,
  Popconfirm,
} from 'antd';
import Editor from '@monaco-editor/react';
import markdown from './markdown.less';
import MarkdownIt from 'markdown-it';
import { useHistory } from 'umi';
const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: (
          <a target="_blank" rel="noopener noreferrer">
            查看
          </a>
        ),
      },
      {
        key: '2',
        label: (
          <a target="_blank" rel="noopener noreferrer">
            设置
          </a>
        ),
      },
      {
        key: '3',
        label: (
          <a target="_blank" rel="noopener noreferrer">
            保存草稿
          </a>
        ),
      },
      {
        key: '4',
        label: (
          <a target="_blank" rel="noopener noreferrer">
            删除
          </a>
        ),
      },
    ]}
  />
);

export default function index(props: any) {
  const [flag, useflag] = useState(true);
  const content = useRef<any>(null);
  const history = useHistory();
  const handChange = (val: any) => {
    // usehtml(content.current.render(val))
  };
  console.log(props.location.state);

  useEffect(() => {
    content.current = new MarkdownIt();
  }, []);
  const handmainRight = () => {
    useflag(false);
  };

  const confirm = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    history.push('/');
  };

  const cancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    message.error('Click on No');
  };

  const { html, toc } = props.location.state;

  console.log(toc[0].text);
  return (
    <div className={styles.editor}>
      {/* 顶部 */}
      <Card className={styles.cardEditor}>
        <div className={styles.editorCard}>
          <div className={styles.editorLeft}>
            <Popconfirm
              title="Are you sure to delete this task?"
              onConfirm={confirm}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
              <Button className={styles.button}>X</Button>
            </Popconfirm>
            <span className="ant-page-header-heading-title">
              <input
                type="text"
                className={styles.antinput}
                placeholder="请输入文章标题"
                value={props.location.state.title}
              />
            </span>
          </div>
          <div className={styles.editorRight}>
            <Button type="primary">
              <span>发 布</span>
            </Button>
            <Dropdown overlay={menu} placement="bottomLeft">
              <span role="img" aria-label="ellipsis">
                <svg
                  viewBox="64 64 896 896"
                  focusable="false"
                  data-icon="ellipsis"
                  width="1em"
                  height="1em"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path>
                </svg>
              </span>
            </Dropdown>
          </div>
        </div>
      </Card>

      <main className={styles.main}>
        <div className={styles.mainLeft}>
          <Editor
            height="900px"
            defaultLanguage="markdown"
            defaultValue={props.location.state.content}
            onChange={handChange}
          />
        </div>
        <div
          className={markdown.markdown}
          id={styles.markID}
          //@ts-ignore
          dangerouslySetInnerHTML={{ __html: html }}
        ></div>
        {flag ? (
          <div className={styles.mainRight}>
            <h2>大纲 </h2>
            <Button className={styles.button} onClick={handmainRight}>
              X
            </Button>
          </div>
        ) : (
          ''
        )}
      </main>
    </div>
  );
}
