import React from 'react';
import styles from './index.less';
export default function index() {
  return (
    <div className={styles.latesactive}>
      <div>
        <span>404</span> | This page could not be found.
      </div>
    </div>
  );
}
