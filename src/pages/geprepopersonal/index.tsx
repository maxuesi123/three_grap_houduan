import styles from './index.less';
import { Form, Input, Button, Checkbox, Tabs } from 'antd';
import { ProCard } from '@ant-design/pro-components';
import { useEffect, useState } from 'react';
export default function accessPage() {
  const { TabPane } = Tabs;
  const [data, useData] = useState([
    '累计发表了 17 篇文章',
    '累计创建了 8 个分类',
    '累计创建了 11 个标签',
    '累计上传了 940 个文件',
    '累计获得了 20 个评论',
  ]);
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={styles.geprepopersonal}>
      <ProCard style={{ marginTop: 8, background: '#f3f3f3' }} gutter={20}>
        <ProCard bordered>
          <div className={styles.left}>
            <div>系统概览</div>
            <div className={styles.leftbottom}>
              <ul>
                {data &&
                  data.map((item, index) => {
                    return (
                      <li key={index}>
                        <span>{item}</span>
                      </li>
                    );
                  })}
              </ul>
            </div>
          </div>
        </ProCard>
        <ProCard
          colSpan={{
            xs: '10%',
            sm: '20%',
            md: '30%',
            lg: '40%',
            xl: '50%',
          }}
          bordered
        >
          <div className={styles.right}>
            <h1>个人资料</h1>
            <Tabs defaultActiveKey="1">
              <TabPane tab="基本设置" key="1">
                <Form
                  name="basic"
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 16 }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                  className={styles.forms}
                >
                  <Form.Item
                    label="用户名"
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your username!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    name={['user', 'email']}
                    label="Email"
                    rules={[{ type: 'email' }]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                      保存
                    </Button>
                  </Form.Item>
                </Form>
              </TabPane>
              <TabPane tab="更新密码" key="2">
                <Form
                  name="basic"
                  labelCol={{ span: 8 }}
                  wrapperCol={{ span: 16 }}
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                >
                  <Form.Item
                    label="原密码"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="新密码"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>
                  <Form.Item
                    label="确认密码"
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>
                  <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit">
                      更新
                    </Button>
                  </Form.Item>
                </Form>
              </TabPane>
            </Tabs>
          </div>
        </ProCard>
      </ProCard>
    </div>
  );
}
