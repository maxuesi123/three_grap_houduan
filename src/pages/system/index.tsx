import './sys.less';

import { Tabs, Radio, Space } from 'antd';
const tabPosition = 'left';
const { TabPane } = Tabs;
import { SettingOne } from './settingOne/settingOne';

// 系统设置
export default function Setting() {
  const dataSet = {
    '1': [
      {
        type: 'input',
        title: '系统地址',
        value: 'creationadmin.shbwyz.com',
        valuekey: 'adminSystemUrl',
      },
      {
        type: 'input',
        title: '后台地址',
        value: 'creationadmin.shbwyz.coma',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: '系统标题',
        value: '小楼又清风',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: 'Logo',
        value:
          '<img height="36" src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png" alt="logo">',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: 'Favicon',
        value:
          'https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png',
        valuekey: 'adminSystemUrlb',
      },
      {
        type: 'textarea',
        title: '页脚信息',
        value:
          '<p>Designed by <a rel="noopener noreferrer" href="https://github.com/fantasticit/wipi" target="_blank">Fantasticit</a> . <a href="https://admin.blog.wipi.tech/" target="_blank" rel="noopener noreferrer">后台管理</a></p><p>Copyright © 2021. All Rights Reserved.</p><p>皖ICP备18005737号</p>',
        valuekey: 'adminSystemUrlb',
      },
    ],
    '3': [
      {
        type: 'input',
        title: '关键词',
        value:
          'JavaScript,TypeScript,Vue.js,微信小程序,React.js,正则表达式,WebGL,Webpack,Docker,MVVM,nginx,java',
        valuekey: 'adminSystemUrl',
      },
      {
        type: 'textarea',
        title: '描述信息',
        value:
          '“小楼又清风”是 fantasticit（https://github.com/fantasticit）的个人小站。本站的文章包括：前端、后端等方面的内容，也包括一些个人读书笔记。',
        valuekey: 'adminSystemUrla',
      },
    ],
    '4': [
      {
        type: 'input',
        title: '百度统计',
        value: '2f616121a4be61774c494d106870f30e',
        valuekey: 'adminSystemUrl',
      },
      {
        type: 'input',
        title: '谷歌分析',
        value: 'G-10SK76KWMS',
        valuekey: 'adminSystemUrla',
      },
    ],
    '6': [
      {
        type: 'input',
        title: 'SMIP地址',
        value: 'smtp.163.com',
        valuekey: 'adminSystemUrl',
      },
      {
        type: 'input',
        title: 'SMTP 端口（强制使用 SSL 连接）',
        value: '465',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: 'SMTP 用户',
        value: 'bwbjwz@163.com',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: 'SMTP 密码',
        value: 'MIQGCLSXRSZQNNPO',
        valuekey: 'adminSystemUrla',
      },
      {
        type: 'input',
        title: '发件人',
        value: 'bwbjwz@163.com',
        valuekey: 'adminSystemUrla',
      },
    ],
  };
  // 国际化
  const onChange = (key: string) => {
    console.log(key);
  };
  return (
    <div className="setting">
      <Tabs tabPosition={tabPosition}>
        <TabPane tab="系统设置" key="1">
          <SettingOne settingOne={dataSet['1']} />
          <button>保存</button>
        </TabPane>

        <TabPane tab="国际化设置" key="2">
          <Tabs defaultActiveKey="1" onChange={onChange}>
            <TabPane tab="en" key="1">
              Content of Tab Pane 1
            </TabPane>
            <TabPane tab="zh" key="2">
              Content of Tab Pane 2
            </TabPane>
          </Tabs>
        </TabPane>

        <TabPane tab="SEO设置" key="3">
          <SettingOne settingOne={dataSet['3']} />
          <button>保存</button>
        </TabPane>

        <TabPane tab="数据统计" key="4">
          <SettingOne settingOne={dataSet['4']} />
          <button>保存</button>
        </TabPane>

        <TabPane tab="OSS设置" key="5">
          <div className="ossOne">
            <div className="ossleft">
              <svg
                viewBox="64 64 896 896"
                focusable="false"
                data-icon="info-circle"
                width="1em"
                height="1em"
                fill="currentColor"
                aria-hidden="true"
              >
                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z"></path>
                <path d="M464 336a48 48 0 1096 0 48 48 0 10-96 0zm72 112h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V456c0-4.4-3.6-8-8-8z"></path>
              </svg>
            </div>
            <div className="ossright">
              <h2>说明</h2>
              <p>请在编辑器中输入您的 oss 配置，并添加 type 字段区分</p>
              <p>
                "type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""
              </p>
            </div>
          </div>
          <div></div>
        </TabPane>

        <TabPane tab="SMTP服务" key="6">
          <SettingOne settingOne={dataSet['6']} />
          <button>保存</button>
          <button className="btn2">测试</button>
        </TabPane>
      </Tabs>
    </div>
  );
}
