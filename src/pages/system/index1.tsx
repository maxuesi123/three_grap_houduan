import React, { FC, useEffect, useState } from 'react';
import { useHistory } from 'umi';
import { ConnectProps, Loading, connect, useSelector, useDispatch } from 'umi';
import './index1.less';
import { Space, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
interface DataType {
  key: string;
  title: string;
  status: string;
  address: string;
  tags: string[];
}
const IndexPage = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  // const dispatch = useDispatch();

  const history = useHistory();

  // const {title,list} = useSelector ((state:any)=>state.article)
  // console.log(useSelector,'999999999999');
  // useEffect(()=>{
  //   dispatch({
  //     type:'article/getarticle',
  //     payload:{page:1,pageSize:10},
  //   })
  // },[])

  // console.log(list,'list--------------------');

  // const handleChangeTitle =()=>{
  //   dispatch({
  //     // 通过dispatch触发reducer
  //     type:'article/changeTitle',
  //     payload:{title:'11111111'}
  //   })
  // }

  const columns: ColumnsType<DataType> = [
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
      render: (text) => <a>{text}</a>,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: '分类',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      render: (_, { tags }) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a>Invite {record.title}</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const data: DataType[] = [
    {
      key: '1',
      title: 'John Brown',
      status: '32',
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      key: '2',
      title: 'Jim Green',
      status: '42',
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    },
  ];

  // 跳转到工作台
  const work = () => {
    history.push('/');
  };

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div>
      <div className="topping">
        <span onClick={work} style={{ color: 'grey' }}>
          工作台
        </span>{' '}
        &emsp; /<span>系统设置</span>
      </div>
      <div>
        {/* <h1>{title}</h1>
        <button onClick={handleChangeTitle}>改变title</button> */}
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
        />
      </div>
    </div>
  );
};

export default IndexPage;

// 6.8 和 6.9
