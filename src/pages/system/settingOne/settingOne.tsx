import './set.less';
import { useCallback, useEffect, useState } from 'react';
import { Input } from 'antd';
export const SettingOne = (props: any) => {
  console.log('@@@@@@@@@@@@');

  const [settingOne, setSettingOne] = useState(null);
  useEffect(() => {
    console.log('&&&&&&&&&&&');
    const { settingOne: data } = { ...props };
    const data1 = JSON.parse(JSON.stringify(data));

    setSettingOne(data1);
  }, []);
  const changeValue = useCallback(
    (e, index) => {
      console.log(e.target.value);
      let abc = settingOne;
      abc[index].value = e.target.value;
      console.log(abc, '121231231231231323');

      setSettingOne(settingOne);
    },
    [settingOne],
  );
  useEffect(() => {
    console.log(settingOne, 'ddddd');
  }, [settingOne]);

  return (
    <div>
      {settingOne &&
        settingOne.map((item, index) => {
          if (item.type === 'input') {
            return (
              <div className="inp" key={item.valuekey}>
                <span>{item.title}</span> <br /> <br />
                <Input
                  className="in"
                  type="text"
                  value={item.value}
                  onInput={(e) => {
                    changeValue(e, index);
                  }}
                />
              </div>
            );
          }
          if (item.type === 'textarea') {
            return (
              <div key={item.valuekey}>
                <span>{item.title}</span> <br /> <br />
                <textarea
                  value={item.value}
                  onChange={(e: any) => {
                    changeValue(e, index);
                  }}
                ></textarea>
              </div>
            );
          }
        })}
    </div>
  );
};
