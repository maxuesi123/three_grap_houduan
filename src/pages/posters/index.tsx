import Posters from './Posters.less';
import { Empty } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { UploadOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { Button, message, Upload } from 'antd';
import React, { useRef } from 'react';
import { InboxOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
import { Breadcrumb } from 'antd';
import { Pushuploads } from '../../services/poster';
//antd官网里的
const props: UploadProps = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};
export default function posterstPage() {
  //ref
  const actionRef = useRef<ActionType>();
  const init = () => {
    actionRef.current && actionRef.current.reload();
  };
  //上传图片
  const uploadlists = async (file: { file: string | Blob }) => {
    //创建了一个空对象实例
    const formData = new FormData();
    //往文件页里添加图片
    formData.append('file', file.file);
    // 调用initactionRef
    await Pushuploads(formData);
    init();
    //添加弹出操作成功
    message.success('操作成功');
  };

  const onChange = (checked: boolean) => {
    setLoading(!checked);
  };
  return (
    <div className="post">
      <div className={Posters.Topnav}>
        &emsp;<span style={{ color: 'grey' }}>工作台</span> /
        <span>海报管理</span>
      </div>
      {/* <Breadcrumb.Item>
      
    </Breadcrumb.Item> */}
      <PageContainer>
        {/* <h1 className="posters"> </h1> */}
        <div className={Posters.list1}>
          {/* <div className={Posters.list2}>
            <span>
              &emsp;&emsp;系统检测到<strong>阿里云OSS配置</strong>未完善,&emsp;
              <a>点我立即完善</a>
            </span>
          </div> */}

          <Dragger {...props} customRequest={uploadlists}>
            <div className="ant-upload-drag-icon">
              <InboxOutlined />
            </div>
            <div className="ant-upload-text">
              点击选择文件或将文件拖拽到此处
            </div>
            <p className="ant-upload-text">文件将上传到OSS,如未配置请先配置</p>
          </Dragger>
        </div>
        <div className={Posters.searchs}>
          <p>
            文件名称:
            <input placeholder="&emsp;请输入文件名称" />
          </p>
          <div className={Posters.but}>
            <Button type="primary">搜索</Button>
            <Button>重置</Button>
          </div>
        </div>
        <div className={Posters.kong}>
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </div>
      </PageContainer>
    </div>
  );
}
function setLoading(arg0: boolean) {
  throw new Error('Function not implemented.');
}
