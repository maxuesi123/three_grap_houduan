import styles from './index.less';
import React, { useState } from 'react';
import Breadcrumb from '../../component/breadcrumb';
import { emailList } from '../../services/email';
import { Table, Button } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import { useRequest } from 'umi';
interface DataType {
  key: React.Key;
  name: string;
  age: number;
  address: string;
}

const columns: ColumnsType<DataType> = [
  {
    title: '发件人',
    width: 100,
    dataIndex: 'name',
    key: 'name',
    fixed: 'left',
  },

  {
    title: '收件人',
    dataIndex: 'address',
    key: '1',
    width: 150,
  },
  {
    title: '主题',
    dataIndex: 'subject',
    key: '2',
    width: 150,
  },
  {
    title: '发送时间',
    dataIndex: 'address',
    key: '3',
    width: 150,
  },

  {
    title: '操作',
    key: 'operation',
    fixed: 'right',
    width: 100,
    align: 'center',
    render: () => <a>删除</a>,
  },
];

const data: DataType[] = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i,
    name: `Edrward ${i}`,
    age: 32,
    address: `London Park no. ${i}`,
  });
}
export default function IndexPage() {
  const { data, error, loading } = useRequest(emailList);

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  // const hasSelected = selectedRowKeys.length > 0;
  function onSelectChange(newSelectedRowKeys: React.Key[]) {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  return (
    <div className={styles.mail}>
      <Breadcrumb children={{ title: '邮件管理', path: '/email' }} />
      <div className={styles.header}>
        <p>
          发件人：
          <input type="text" placeholder="请输入发件人" />{' '}
        </p>
        <p>
          收件人： <input type="text" placeholder="请输入收件人" />
        </p>
        <p>
          主题： <input type="text" placeholder="请输入主题" />
        </p>

        <div className={styles.but}>
          <Button type="primary">搜索</Button>
          <Button>重置</Button>
        </div>
      </div>
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data && data[0]}
        scroll={{ x: 1500, y: 300 }}
      />
    </div>
  );
}
