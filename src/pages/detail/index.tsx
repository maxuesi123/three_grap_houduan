import React from 'react';
import Editor from '@monaco-editor/react';
import { useState, useEffect, useRef } from 'react';
import { markdown } from '../detail/markdown.less';
import MarkdownIt from 'markdown-it';
import detail from '../detail/detail.less';
// import {DetailList} from '../../services/article';
// import  Detailtext    from './Detailtext';
import text from './Detailtext';
export default function index(props: any) {
  console.log(props);
  const [html, setHtml] = useState('');
  const content = useRef<any>(null);
  // let data=props.location.state;
  // console.log(data)
  const handleChange = (val: any) => {
    setHtml(content.current.render(val));
  };
  useEffect(() => {
    content.current = new MarkdownIt();
  }, []);
  return (
    <div className={detail.Bigbox}>
      <div className={detail.Boxleft}>
        <Editor
          language="markdown"
          theme="vs-dark"
          onChange={handleChange}
          value={text}
        />
      </div>
      <div
        className={markdown.markdown}
        dangerouslySetInnerHTML={{ __html: html }}
      ></div>
      <div className={detail.Boxright}></div>
    </div>
  );
}
