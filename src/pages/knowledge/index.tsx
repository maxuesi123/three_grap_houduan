import Study from './study.less';
import { Button, Select, Drawer, Form, Input, Switch } from 'antd';
import { useState, useEffect, useRef } from 'react';
import { Upload, message, Card } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { knowledgeList, knowledgeList1 } from '../../services/knowledge';
import { UploadOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import type { PaginationProps } from 'antd';
import { Pagination } from 'antd';
import { Pushupload } from '../../services/knowledge';
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { Avatar } from 'antd';
import { ActionType } from '@ant-design/pro-components';
// import { init } from 'echarts';
// import {
//   EditOutlined,
//   CloudDownloadOutlined,
//   SettingOutlined,
//   DeleteOutlined,
// } from '@ant-design/icons';
// import { Pagination } from 'antd';
interface items {
  cover: string;
  publishAt: string;
  views: string;
  title: any;
  summary: any;
  url: string;
  type: string;
  createAt: string;
}
const { Meta } = Card;

// import { UploadOutlined } from '@ant-design/icons';
// import type { UploadFile } from 'antd/es/upload/interface';
const { Option } = Select;
const { Dragger } = Upload;
const layout = {
  labelCol: { span: 100 },
  wrapperCol: { span: 300 },
};
//分页
const onChanges: PaginationProps['onChange'] = (pageNumber) => {
  console.log('Page: ', pageNumber);
};
const onShowSizeChange: PaginationProps['onShowSizeChange'] = (
  current,
  pageSize,
) => {
  console.log(current, pageSize);
};
//上传文件

const props: UploadProps = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

/* eslint-enable no-template-curly-in-string */
//开关
const onChange = (checked: boolean) => {
  console.log(`switch to ${checked}`);
};

export default function knowledgelePage() {
  const [visible, setVisible] = useState(false);
  const [childrenDrawer, setChildrenDrawer] = useState(false);

  // const [data, setdata] = useState([]);

  const [loading, setLoading] = useState(true);

  const [page, usepage] = useState(1); //当前页
  const [pageSize, usepageSize] = useState(12); //每页多少条
  const [pagelength, usepagelength] = useState(0);
  //模糊搜索
  const [data, setdata] = useState([]); //外面的数据
  const [valdatas, valDatass] = useState('');
  const [datas, setdata1] = useState([]);
  //抽屉里的搜索
  const [data1, setdatas] = useState([]); //抽屉里的数据
  const [valdatas1, valDatass1] = useState('');
  const [datas1, setdata2] = useState([]);
  const [dataLength, usedatalength] = useState(0);
  const actionRef = useRef<ActionType>();
  const init = () => {
    actionRef.current && actionRef.current.reload();
  };
  //上传图片
  const uploadlist = async (file: { file: string | Blob }) => {
    const formData = new FormData();
    formData.append('file', file.file);
    await Pushupload(formData);
    init();
    message.success('操作成功');
  };
  const onChange = (checked: boolean) => {
    setLoading(!checked);
  };
  //异步函数
  useEffect(() => {
    getdatas();
    // getdata1();
  }, []);
  useEffect(() => {
    getdata2();
  }, []);
  const getdatas = async () => {
    const res = await knowledgeList({ page: page, pageSize: pageSize });
    if (res) {
      setdata(res.data[0]);
      //模糊搜索
      setdata1(res.data[0]);
      usedatalength(res.data[1]);
    } else {
      console.log('失败');
    }
  };
  const getdata2 = async () => {
    const res = await knowledgeList1({ page: page, pageSize: pageSize });
    console.log(page, '222');

    if (res) {
      setdatas(res.data[0]);
      usepagelength(res.data[1]); //分页
      // 抽屉里的搜索
      setdata2(res.data[0]);
    } else {
      console.log('失败');
    }
  };
  //分页
  const onChangess = (page: number) => {
    usepage(page);
    console.log(page);

    getdata2();
  };
  //模糊搜素事件
  const searchs = (e: any) => {
    valDatass(e.target.value);
    console.log(valdatas, '///////');
  };
  const search1 = () => {
    const val1 = datas.filter((item, index) => {
      return item.title.includes(valdatas);
    });
    setdata(val1);
  };
  //抽屉里的搜索
  const search2 = (e: any) => {
    valDatass1(e.target.value);
    console.log(valdatas, '///////');
  };
  const search3 = () => {
    const val2 = datas1.filter((item, index) => {
      return item.type.includes(valdatas1);
    });
    setdatas(val2);
  };
  //抽屉开关
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const showChildrenDrawer = () => {
    setChildrenDrawer(true);
  };

  const onChildrenDrawerClose = () => {
    setChildrenDrawer(false);
  };

  const onFinish = (values: any) => {
    console.log(values);
  };

  return (
    <div className={Study.study}>
      <PageContainer>
        <div className={Study.Topnav}>
          &emsp;<span style={{ color: 'grey' }}>工作台</span> /
          <span>知识小册</span>
        </div>
        <div className={Study.searchs}>
          <div className={Study.search1}>
            <p>
              文件名称:
              <input placeholder="&emsp;请输入文件名称" onChange={searchs} />
            </p>
            <p>
              &emsp;&emsp; 状态:&emsp;
              <Select style={{ width: 180 }} allowClear>
                <Option value="lucy">已发布</Option>
                <Option value="lucy">草稿</Option>
              </Select>
            </p>
          </div>
          <div className={Study.but}>
            <Button type="primary" onClick={search1}>
              搜索
            </Button>
            <Button>重置</Button>
          </div>
        </div>
        <div className={Study.imgs}>
          <div className={Study.ww}>
            <div className={Study.bus}></div>
            <Button type="primary" onClick={showDrawer}>
              ＋新建
            </Button>
          </div>
          <div className={Study.renders}>
            {data &&
              data.map((item: items, index) => {
                return (
                  <div key={index}>
                    <Card
                      style={{ width: 230, marginLeft: 30, marginTop: 10 }}
                      cover={<img alt="example" src={item.cover} />}
                      actions={[
                        <SettingOutlined key="setting" />,
                        <EditOutlined key="edit" />,
                        <EllipsisOutlined key="ellipsis" />,
                      ]}
                    >
                      <Meta title={item.title} description={item.summary} />
                    </Card>
                  </div>
                );
              })}
          </div>
        </div>
        <div className={Study.fen}>
          <p>
            <Pagination
              showQuickJumper
              defaultCurrent={1}
              current={page}
              pageSize={pageSize}
              total={dataLength}
              onChange={onChangess}
            />
          </p>
        </div>
        {/* 抽屉 */}
        <div className={Study.xin}>
          <Drawer
            title="新建知识库"
            width={600}
            closable={false}
            onClose={onClose}
            visible={visible}
          >
            <Form {...layout} name="nest-messages" onFinish={onFinish}>
              <Form.Item name={['user', 'name']} label="名称">
                <Input />
              </Form.Item>
              <Form.Item name={['user', 'introduction']} label="描述">
                <Input.TextArea />
              </Form.Item>
              <Form.Item name={['user', 'introduction']} label="评论">
                <Switch defaultChecked />
              </Form.Item>
              {/* 上传图片 */}
              <Form.Item name={['user', 'introduction']} label="封面">
                <Dragger customRequest={uploadlist} {...props}>
                  <div className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </div>
                  <div className="ant-upload-text">
                    点击选择文件或将文件拖拽到此处
                  </div>
                  <p className="ant-upload-text">
                    文件将上传到OSS,如未配置请先配置
                  </p>
                </Dragger>
              </Form.Item>
              <Form.Item name={['user', 'name']} label="">
                <Input />
              </Form.Item>
            </Form>
            <Button onClick={showChildrenDrawer} style={{ width: 80 }}>
              选择文件
            </Button>
            <div className={Study.aa}>
              <Button>取消</Button>&emsp;
              <Button>创建</Button>
            </div>
            <Drawer
              title="文件管理"
              width={800}
              closable={false}
              onClose={onChildrenDrawerClose}
              visible={childrenDrawer}
            >
              <div className={Study.inp}>
                文件名称:
                <Input placeholder="" onChange={search2} />
                &emsp; 文件类型:
                <Input placeholder="" />
                <div className={Study.buts}>
                  <Button type="primary" onClick={search3}>
                    搜索
                  </Button>
                  &emsp;
                  <Button>重置</Button>
                </div>
              </div>
              <div className={Study.wen}>
                <Upload {...props}>
                  <Button icon={<UploadOutlined />} style={{ width: 100 }}>
                    上传文件
                  </Button>
                </Upload>
              </div>
              <div className={Study.tu}>
                {data1 &&
                  data1.map((item: items, index) => {
                    return (
                      <Card
                        size="small"
                        hoverable
                        style={{ width: 150, height: 176, marginLeft: 15 }}
                        cover={<img alt="example" key={index} src={item.url} />}
                      >
                        <Meta
                          style={{
                            width: 150,
                            height: 40,
                            marginLeft: -14,
                            paddingLeft: 14,
                          }}
                          title={item.type}
                        />
                      </Card>
                    );
                  })}
                <div className={Study.fens}>
                  <p>
                    <Pagination
                      showQuickJumper
                      defaultCurrent={2}
                      current={page}
                      pageSize={pageSize}
                      total={pagelength}
                      onChange={onChangess}
                    />
                  </p>
                </div>
              </div>
            </Drawer>
          </Drawer>
        </div>
      </PageContainer>
    </div>
  );
}
