import styles from './index.less';
import FileLess from './file.module.less';
import { fileList } from '../../services/user';
import { useState, useEffect } from 'react';
import { Button, Drawer, Pagination } from 'antd';

interface items {
  url: string;
  type: string;
  createAt: string;
  item: any;
  filename: string;
  originalname: string;
  size: string;
}

export default function filePage() {
  const [visible, setVisible] = useState(false);
  const [page, usepage] = useState(1); //当前页
  const [pageSize, usepageSize] = useState(12); //当前页12条
  const [filesLength, usefilesLength] = useState(0);
  const [data, setdata] = useState([]);
  const [list, setlist] = useState([]);
  useEffect(() => {
    getdata();
  }, []);

  const getdata = async () => {
    const res = await fileList({ page: page, pageSize: pageSize });
    if (res) {
      // console.log(res.data[0]);
      setdata(res.data[0]);
      usefilesLength(res.data[1]);
    } else {
      console.log('失败');
    }
  };

  // const showDrawer = () => {
  //   setVisible(true);
  // };

  const onClose = () => {
    setVisible(false);
  };

  const handlDrawe = (item: any) => {
    console.log([item]);
    setlist([item]);
    console.log(list);

    setVisible(true);
  };
  //改变当前页
  const handPigeSize = (page: number) => {
    usepage(page);
    getdata();
  };
  return (
    <div className={FileLess.files}>
      <div className={FileLess.header}>
        <p>
          <span>工作台</span>/<span>文件管理</span>
        </p>
      </div>
      <div className={FileLess.main}>
        <div className={FileLess.main_top}>
          <p>
            {/* <svg
              viewBox="0 0 1024 1024"
              focusable="false"
              data-icon="inbox"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <path d="M885.2 446.3l-.2-.8-112.2-285.1c-5-16.1-19.9-27.2-36.8-27.2H281.2c-17 0-32.1 11.3-36.9 27.6L139.4 443l-.3.7-.2.8c-1.3 4.9-1.7 9.9-1 14.8-.1 1.6-.2 3.2-.2 4.8V830a60.9 60.9 0 0060.8 60.8h627.2c33.5 0 60.8-27.3 60.9-60.8V464.1c0-1.3 0-2.6-.1-3.7.4-4.9 0-9.6-1.3-14.1zm-295.8-43l-.3 15.7c-.8 44.9-31.8 75.1-77.1 75.1-22.1 0-41.1-7.1-54.8-20.6S436 441.2 435.6 419l-.3-15.7H229.5L309 210h399.2l81.7 193.3H589.4zm-375 76.8h157.3c24.3 57.1 76 90.8 140.4 90.8 33.7 0 65-9.4 90.3-27.2 22.2-15.6 39.5-37.4 50.7-63.6h156.5V814H214.4V480.1z"></path>
            </svg> */}

            {/* <br /> */}
            <span>
              系统检测到<b>阿里云OSS配置</b>未完善
            </span>
            {/* <br /> */}
            <span>&emsp;点我立即完善</span>
          </p>
        </div>
        <div className={FileLess.main_bom}>
          <div className={FileLess.bom_top}>
            &emsp; 文件名称： <input type="text" />
            &emsp; &emsp; 文件类型：
            <input type="text" />
            <p>
              <button className={FileLess.select}>搜索</button>
              <button>重置</button>
            </p>
          </div>
          <div id="small" style={{ background: '#e7eaee' }}>
            `
          </div>
          <div className={FileLess.bom_bom}>
            {data &&
              data.map((item: items, index) => {
                return (
                  <div key={index} className={FileLess.file_div}>
                    <dl onClick={() => handlDrawe(item)}>
                      <dt>
                        <img src={item.url} alt="" />
                      </dt>
                      <dd>{item.type}</dd>
                      <dd>{item.createAt}</dd>
                    </dl>
                  </div>
                );
              })}
          </div>
          {list && list.length
            ? list.map((item1: items, index1) => {
                return (
                  <div key={index1}>
                    {/* <Button type="primary" onClick={showDrawer}>
                      
                    </Button> */}
                    <Drawer
                      title="文件信息"
                      placement="right"
                      onClose={onClose}
                      visible={visible}
                    >
                      <p className={FileLess.p1}>
                        <img src={item1.url} alt="" />
                      </p>
                      <p>文件名称：{item1.originalname}</p>
                      <p>存储路径：{item1.filename}</p>
                      <p>文件类型：{item1.type}</p>
                      <p>文件大小：{item1.size}</p>
                      访问连接：
                      <input type="text" value={item1.url} />
                    </Drawer>
                  </div>
                );
              })
            : ''}

          <div>
            <Pagination
              defaultCurrent={6}
              total={filesLength}
              current={page}
              pageSize={pageSize}
              onChange={handPigeSize}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
