import styles from './index.less';
//@ts-ignore
import { useRequest, useHistory } from 'umi';
import { Form, Input, Button } from 'antd';
import { register } from '@/services/user';
import registeredimg from './img/img.png';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
const { confirm } = Modal;
export default function IndexPage() {
  const history = useHistory();
  const { run, loading } = useRequest(register, { manual: true });
  //注册
  const onResigterFinish = async (values: any) => {
    let res = await run(values);
    console.log(res);
    confirm({
      title: '注册成功',
      icon: <ExclamationCircleOutlined />,
      content: '是否跳转至登录？',
      onOk() {
        history.replace('/login');
      },
    });
  };

  //点击去登录页
  const handelogin = () => {
    console.log(111);
    history.push('/login');
  };
  return (
    <div className={styles.registeredPage}>
      <div className={styles.center}>
        <div className={styles.left}>
          <img src={registeredimg} alt="" />
        </div>
        <div className={styles.right}>
          <div className={styles.register}>
            <h3>访客注册</h3>
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onResigterFinish}
              autoComplete="off"
              className={styles.loginform}
            >
              <Form.Item
                label="账户"
                name="name"
                rules={[
                  { required: true, message: 'Please input your username!' },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="密码"
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
                hasFeedback
              >
                <Input.Password />
              </Form.Item>
              <Form.Item
                label="确认"
                name="confirm"
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please confirm your password!',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        new Error(
                          'The two passwords that you entered do not match!',
                        ),
                      );
                    },
                  }),
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit" loading={loading}>
                  注册
                </Button>
              </Form.Item>
            </Form>
            <br />
            <p>
              Or <span onClick={handelogin}>去登录</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
