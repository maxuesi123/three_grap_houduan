import { pageList } from '../../services/page';
import page from '../pagemanagement/page.less';
import { useEffect, useState } from 'react';
import React from 'react';
import { Input, Button, Table, Select, Badge, Tag } from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import type { TableRowSelection } from 'antd/lib/table/interface';

export default function pagemanagementPage() {
  const status: any = {
    publish: {
      status: '已发布',
      color: 'green',
    },
    draft: {
      status: '草稿',
      color: '#faad14',
    },
  };
  const [data, setdata] = useState([]);

  useEffect(() => {
    getdata();
  }, []);
  const getdata = async () => {
    const res = await pageList({ page: 1, pageSize: 12 });
    console.log(res.data);
    if (res) {
      setdata(res.data[0]);
      console.log(res.data[0], 'res.data[0]');
    } else {
      console.log('sibai');
    }
  };
  interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
  }

  const columns: ColumnsType<any> = [
    {
      title: '名称',
      width: 100,
      dataIndex: 'name',
      key: 'id',
      fixed: 'left',
    },
    {
      title: '路径',
      dataIndex: 'views',
      key: 'id',
      width: 150,
    },
    {
      title: '顺序',
      dataIndex: 'order',
      key: 'id',
      width: 150,
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'id',
      width: 150,
      render(text: string) {
        return text ? (
          <Badge
            style={{ backgroundColor: '#52c41a' }}
            count={text}
            key={text}
          ></Badge>
        ) : (
          ''
        );
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'id',
      width: 150,
      render(text: string) {
        return text ? (
          //@ts-ignore
          <Badge color={status[text].color}>
            <span className="span_style___lH_9g"></span>
            {status[text].status}
          </Badge>
        ) : (
          ''
        );
      },
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'id',
      width: 150,
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: () => (
        <p>
          <div className="ant-divider ant-divider-vertical">
            <a>编辑</a>
          </div>
          <div className="ant-divider ant-divider-vertical">
            {' '}
            <a>下线</a>
          </div>
          <div className="ant-divider ant-divider-vertical">
            <a>查看访问</a>
          </div>
          <div className="ant-divider ant-divider-vertical">
            <a>删除</a>
          </div>
        </p>
      ),
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  //单选反选
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };

  console.log(data);
  return (
    <div className={page.box}>
      <div className={page.antone}>
        <span>
          标题: <input type="text" placeholder="请输入标题" />
        </span>
        <span>
          路径:
          <input type="text" placeholder="请输入页面路径" />
        </span>
        <span>
          状态:{' '}
          <select name="" id="">
            <option value="">已发布</option>
            <option value="">草稿</option>
          </select>
        </span>
        <div id={page.but}>
          <button className={page.button1}>搜索 </button>
          <button className={page.button2}>重置 </button>
        </div>
      </div>

      <Button type="primary" className={page.button3}>
        +&emsp;新建
      </Button>

      {/* {data && data.length
          ? data.map((item) => {
              return (
                <li key={item.id}>
                  {item.title}
                  <img alt="" src={item.cover} style={{ width: 50 }}></img>
                </li>
              );
            })
          : null} */}
      <div className={page.BoxCenter}>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          scroll={{ x: 1500, y: 700 }}
          // scroll={{ x: 1500, }}
        />
      </div>
    </div>
  );
}
