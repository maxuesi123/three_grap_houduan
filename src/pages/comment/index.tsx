import React, { useState } from 'react';
import { useRequest } from 'umi';
import { commentList } from '../../services/comment';
import {
  Table,
  Tag,
  Space,
  Button,
  Form,
  Input,
  Checkbox,
  Select,
  Popover,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import styles from './index.less';

interface DataType {
  key: string;
  name: string;
  age: number | string;
  address: string;
}
const { Option } = Select;

// const data: DataType[] = [
//   {
//     key: '1',
//     name: '通过',
//     age: 111,
//     address: '11@qq.com',
//   },
//   {
//     key: '2',
//     name: '通过',
//     age: 123,
//     address: 'London No. 1 Lake Park',
//   },
//   {
//     key: '3',
//     name: '通过',
//     age: 'aa',
//     address: 'aaa',
//   },
//   {
//     key: '4',
//     name: '通过',
//     age: 42,
//     address: '11@qq.com',
//   },
//   {
//     key: '5',
//     name: '通过',
//     age: 32,
//     address: 'Sidney No. 1 Lake Park',
//   },
//   {
//     key: '6',
//     name: '通过',
//     age: 42,
//     address: 'London No. 1 Lake Park',
//   },
//   {
//     key: '7',
//     name: '通过',
//     age: 32,
//     address: 'Sidney No. 1 Lake Park',
//   },
// ];
const columns: ColumnsType<DataType> = [
  {
    title: '状态',
    dataIndex: 'pass',
    key: 'pass',
    fixed: 'left',
    width: '100px',
    render: (text) => (
      <div>
        <span className={styles.span_style}></span>
        通过
      </div>
    ),
  },
  {
    title: '称呼',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '联系方式',
    dataIndex: 'content',
    key: 'content',
  },
  {
    title: '原始内容',
    dataIndex: 'content',
    key: 'content',
    render: (text, record) => {
      return (
        <Popover content={text} title="Title">
          <a>查看内容</a>
        </Popover>
      );
    },
  },
  {
    title: 'HTML内容',
    dataIndex: 'content',
    key: 'content',
    render: (text, record) => {
      return (
        <Popover content={text} title="Title">
          <a>查看内容</a>
        </Popover>
      );
    },
  },
  {
    title: '管理文章',
    dataIndex: 'address',
    key: 'address',
    render: (text, record) => <a>文章</a>,
  },
  {
    title: '创建时间',
    dataIndex: 'createAt',
    key: 'createAt',
  },
  {
    title: '父级评论',
    dataIndex: 'address',
    key: 'address',
    render: (text) => <div>无</div>,
  },
  {
    title: '操作',
    key: 'action',
    fixed: 'right',
    width: 260,
    render: (_, record) => (
      <Space size="middle">
        <a>通过</a>
        <a>拒绝</a>
        <a>回复</a>
        <a>删除</a>
      </Space>
    ),
  },
];
const App: React.FC = () => {
  const { data, error, loading } = useRequest(commentList);
  console.log(data && data[0], 'daa');
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const hasSelected = selectedRowKeys.length > 0;
  function onSelectChange(newSelectedRowKeys: React.Key[]) {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  return (
    <div className={styles.comment_box}>
      <div className={styles.header_box}>
        <div className={styles.input_box}>
          称呼:
          <Input placeholder="请输入称呼" style={{ width: '230px' }} />
          Email:
          <Input placeholder="请输入联系方式" style={{ width: '230px' }} />
          状态:{' '}
          <Select
            defaultValue="已通过"
            style={{ width: 200 }}
            // onChange={handleChange}
          >
            <Option value="已通过">已通过</Option>
            <Option value="未通过">未通过</Option>
          </Select>
        </div>
        <div className={styles.button_box}>
          <Button type="primary" style={{ marginRight: '30px' }}>
            搜索
          </Button>
          <Button>重置</Button>
        </div>
      </div>
      <div className={styles.content_box}>
        <div className={styles.content_head}>
          <div>
            {' '}
            <span style={{ marginLeft: 8, width: '600px' }}>
              {hasSelected ? (
                <>
                  {' '}
                  <Button>通过</Button> <Button>拒绝</Button>{' '}
                  <Button type="primary" danger ghost>
                    删除
                  </Button>
                </>
              ) : (
                ''
              )}
            </span>
          </div>
          <div>
            <svg
              viewBox="64 64 896 896"
              focusable="false"
              data-icon="reload"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path>
            </svg>
          </div>
        </div>
        <Table
          rowSelection={rowSelection}
          style={{ marginTop: '20px' }}
          columns={columns}
          dataSource={data && data[0]}
          scroll={{ x: 1500 }}
          pagination={{ pageSize: 5, showSizeChanger: true }}
        />
      </div>
    </div>
  );
};

export default App;
