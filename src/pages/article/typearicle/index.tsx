import typearicle from '../typearicle/typearicle.less';
import { Input, Button } from 'antd';
export default function typearticlePage() {
  return (
    <div>
      <div className={typearicle.Topnav}>
        <span style={{ color: 'grey' }}>&emsp; 工作台</span> &emsp; /
        <span>&emsp;所有文章</span> &emsp; /<span>&emsp;标签文章</span>
      </div>
      <div className={typearicle.labelBox}>
        <h3>添加分类</h3>
        <hr />
        <Input
          className={typearicle.ClassifyInput}
          placeholder="输入标签名称"
        />
        <Input
          className={typearicle.ClassifyInput2}
          placeholder="输入标签值(请输入英文,作为路由使用)"
        />

        <Button type="primary" className={typearicle.ClassifyButton}>
          保存
        </Button>
      </div>
      <div className={typearicle.labelBox2}>
        <h3>所有分类</h3>
        <hr />
        <p>
          <button>前端</button>
          <button>后端</button>
          <button>阅读</button>
          <button>LeetCode</button>
          <button>Linux</button>
          <button>CSS</button>
        </p>
      </div>
    </div>
  );
}
