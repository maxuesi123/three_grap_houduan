import allarticle from './allarticle.less';
import { articleList } from '../../../services/article';
import { useEffect, useState } from 'react';

import React from 'react';
import {
  Input,
  Modal,
  Button,
  Table,
  Select,
  Badge,
  Tag,
  Popconfirm,
  message,
  Breadcrumb,
} from 'antd';
import type { ColumnsType } from 'antd/lib/table';
import type { TableRowSelection } from 'antd/lib/table/interface';
import { deleteArticleList, DetailList } from '../../../services/article';
import { useHistory } from 'umi';
export default function allarticlePage() {
  const status: any = {
    publish: {
      status: '已发布',
      color: 'green',
    },
    draft: {
      status: '草稿',
      color: '#faad14',
    },
  };
  //查看访问
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const history = useHistory();
  //删除
  const confirm = async (item: any) => {
    console.log(item, '123');
    let time = null;
    const res = await deleteArticleList(item.id);
    console.log(res);
    if (res.data) {
      message.info('删除成功');
    }
    time = setTimeout(() => {
      window.location.reload();
    }, 5000);
  };
  //编辑
  const Edit = async (item: any) => {
    console.log(item, '123');
    const res = await DetailList(item.id);
    console.log(res);
    if (res.data) {
      console.log(item, 'itemmmmmm');
      history.push('/detail', item);
    }
  };
  //获取数据
  const [data, setdata] = useState([]);
  useEffect(() => {
    getdata();
  }, []);
  const getdata = async () => {
    const res = await articleList({ page: 1, pageSize: 12 });
    console.log(res.data);
    if (res) {
      setdata(res.data[0]);
      console.log(res.data[0], 'res.data[0]');
    } else {
      console.log('sibai');
    }
  };
  interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
  }

  const columns: ColumnsType<any> = [
    {
      title: '标题',
      width: 100,
      dataIndex: 'title',
      key: 'id',
      fixed: 'left',
    },
    {
      title: '状态',
      width: 100,
      dataIndex: 'status',
      key: 'id',
      render(text: string) {
        return text ? (
          //@ts-ignore

          <Badge color={status[text].color}>
            <span className="span_style___lH_9g"></span>
            {status[text].status}
          </Badge>
        ) : (
          ''
        );
      },
    },
    {
      title: '分类',
      dataIndex: 'category',
      key: 'id',
      width: 150,
      render(text: any) {
        return text ? (
          <Tag key={text} color="volcano">
            {text.label}
          </Tag>
        ) : (
          ''
        );
      },
    },
    {
      title: '标签',
      dataIndex: 'tags',
      key: 'id',
      width: 150,
      render(text: any) {
        return text[0] ? (
          <div>
            {text && text.length
              ? text.map((item: any, index: any) => {
                  return (
                    <Tag key={index} color="yellow">
                      {item.label}
                    </Tag>
                  );
                })
              : null}
          </div>
        ) : (
          ''
        );
      },
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'id',
      width: 150,
      render(text: string) {
        return text ? (
          <Badge
            style={{ backgroundColor: '#52c41a' }}
            count={text}
            key={text}
          ></Badge>
        ) : (
          ''
        );
      },
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      key: 'id',
      width: 150,
      render(text: string) {
        return text ? (
          <Badge
            style={{ backgroundColor: 'deepplink' }}
            count={text}
            key={text}
          ></Badge>
        ) : (
          ''
        );
      },
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'id',
      width: 150,
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: (page: any, item: any) => (
        <p>
          <div className="ant-divider ant-divider-vertical">
            <Popconfirm
              key="popconfirm"
              title={`确认跳转吗?`}
              okText="是"
              cancelText="否"
              onConfirm={() => {
                Edit(item);
              }}
            >
              <a>编辑</a>
            </Popconfirm>
          </div>
          <div className="ant-divider ant-divider-vertical">
            {' '}
            <a>撤销首焦</a>
          </div>
          <div className="ant-divider ant-divider-vertical">
            <a onClick={showModal}>查看访问</a>

            <Modal
              title="Basic Modal"
              visible={isModalVisible}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <h1>访问统计</h1>
            </Modal>
          </div>
          <div className="ant-divider ant-divider-vertical">
            <Popconfirm
              key="popconfirm"
              title={`确认删除吗?`}
              okText="是"
              cancelText="否"
              onConfirm={() => {
                confirm(item);
              }}
            >
              <a>删除</a>
            </Popconfirm>
          </div>
        </p>
      ),
    },
  ];
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  //单选反选
  const rowSelection: TableRowSelection<DataType> = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys) => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((_, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setSelectedRowKeys(newSelectedRowKeys);
        },
      },
    ],
  };

  console.log(data);

  return (
    <div className={allarticle.box}>
      <div className={allarticle.Topnav}>
        <span style={{ color: 'grey' }}>工作台</span> &emsp; /
        <span>用户管理</span>
      </div>
      <div className={allarticle.antone}>
        <span>
          标题: <input type="text" placeholder="请输入标题" />
        </span>
        <span>
          状态:
          <select name="" id="">
            <option value="">已发布</option>
            <option value="">草稿</option>
          </select>
        </span>
        <span>
          分类:{' '}
          <select name="" id="">
            <option value="">前端</option>
            <option value="">后端</option>
            <option value="">阅读</option>
            <option value="">LeetCode</option>
          </select>
        </span>
        <div id={allarticle.but}>
          <button className={allarticle.button1}>搜索 </button>
          <button className={allarticle.button2}>重置 </button>
        </div>
      </div>

      <Button type="primary" className={allarticle.button3}>
        +&emsp;新建
      </Button>

      {/* {data && data.length
          ? data.map((item) => {
              return (
                <li key={item.id}>
                  {item.title}
                  <img alt="" src={item.cover} style={{ width: 50 }}></img>
                </li>
              );
            })
          : null} */}
      <div className={allarticle.BoxCenter}>
        <Table
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          scroll={{ x: 1500, y: 700 }}
          // scroll={{ x: 1500, }}
        />
      </div>
    </div>
  );
}
