import React, { FC } from 'react';
// import '../../../../src/App.css';
import tagsarticle from '../tagsarticle/tagsarticle.less';
import { Input, Button } from 'antd';
export default function tagsaarticlePage() {
  return (
    <div>
      <div className={tagsarticle.Topnav}>
        <span style={{ color: 'grey' }}>&emsp; 工作台</span> &emsp; /
        <span>&emsp;所有文章</span> &emsp; /<span>&emsp;分类管理</span>
      </div>
      <div className={tagsarticle.Classify1}>
        <h3>添加分类</h3>
        <hr />
        <Input
          className={tagsarticle.ClassifyInput}
          placeholder="输入分类名称"
        />
        <Input
          className={tagsarticle.ClassifyInput2}
          placeholder="输入分类值(请输入英文,作为路由使用)"
        />

        <Button type="primary" className={tagsarticle.ClassifyButton}>
          保存
        </Button>
      </div>
      <div className={tagsarticle.Classify2}>
        <h3>所有分类</h3>
        <hr />
        <p>
          <button>前端</button>
          <button>后端</button>
          <button>阅读</button>
          <button>LeetCode</button>
          <button>Linux</button>
          <button>CSS</button>
        </p>
      </div>
    </div>
  );
}
