import React from 'react';
import { Breadcrumb, Affix } from 'antd';
import { useEffect, useState } from 'react';
import './index.less';
interface ChildrenType {
  title: string;
  path: string;
}
interface UserStateType {
  name?: string;
  role?: string;
}
interface Type {
  children?: ChildrenType;
  title?: string;
  path?: string;
}

// let user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user")!)
//   console.log(user, "user");

// const App: React.FC<type> = ({ children }) => (
//   <Affix offsetTop={48}>
//     <header>
//       <Breadcrumb>
//         <Breadcrumb.Item>
//           <a href="/">工作台</a>
//           {children ? (
//             ''
//           ) : (
//             <span>
//               <b>您好,{user.name}</b>
//               <p>您的角色: {user.role==="admin"?"管理员":"访客"}</p>
//             </span>
//           )}
//         </Breadcrumb.Item>
//         {children ? (
//           <Breadcrumb.Item>
//             <a href={children.path}>{children.title}</a>
//           </Breadcrumb.Item>
//         ) : (
//           ''
//         )}
//       </Breadcrumb>
//     </header>
//   </Affix>
// );

// export default App;

export default function App({ children }: Type) {
  console.log(children, 'children');
  let [user, getUser] = useState<UserStateType>();
  useEffect(() => {
    getUser(
      localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')!),
    );
  }, []);
  console.log(user, 'user');

  return (
    <Affix offsetTop={48}>
      <header>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/">工作台</a>
          </Breadcrumb.Item>
          {children ? (
            <Breadcrumb.Item>
              <a href={children.path}>{children.title}</a>
            </Breadcrumb.Item>
          ) : (
            ''
          )}
        </Breadcrumb>
        {children ? (
          ''
        ) : (
          <span>
            <b>您好,{user && user.name}</b>
            <p>您的角色: {user && user.role === 'admin' ? '管理员' : '访客'}</p>
          </span>
        )}
      </header>
    </Affix>
  );
}
