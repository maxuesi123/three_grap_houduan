import styles from './index.less';
import { Dropdown, Menu, Button } from 'antd';
import { useHistory } from 'umi';
export default function accessPage() {
  const history = useHistory();

  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a onClick={() => handnewWenS(history.push('/amEditor'))}>
              新建文章-协同编译器
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.aliyun.com"
            >
              新建文章
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.luohanacademy.com"
            >
              新建页面
            </a>
          ),
        },
      ]}
    />
  );
  const handnewWenS = () => {
    console.log(11);
    console.log(history);
    // history.push('/amEditor')
    history.push('/amEditor');
    window.location.href = 'http://localhost:8000/amEditor';
  };
  return (
    <div className={styles.new}>
      <div className={styles.headnew}>
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png"
          alt=""
        />
        <p>管理后台</p>
      </div>
      <Dropdown overlay={menu} placement="bottom" arrow>
        <Button type="primary" size="large" className={styles.button}>
          <span role="img" aria-label="plus" className="anticon anticon-plus">
            <svg
              viewBox="64 64 896 896"
              focusable="false"
              data-icon="plus"
              width="1em"
              height="1em"
              fill="currentColor"
              aria-hidden="true"
            >
              <defs>
                <style></style>
              </defs>
              <path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path>
              <path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path>
            </svg>
          </span>
          <span>新建</span>
        </Button>
      </Dropdown>
    </div>
  );
}
