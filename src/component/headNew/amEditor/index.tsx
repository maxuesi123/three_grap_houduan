import React from 'react';
import { useState } from 'react';
import { Card } from 'antd';
import styles from './index.less';
import { Button, Dropdown, Menu, message, Popconfirm } from 'antd';
import { useHistory } from 'umi';
import menu from 'antd/lib/menu';

export default function index() {
  const [flag, useflag] = useState(true);
  const history = useHistory();
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a target="_blank" rel="noopener noreferrer">
              查看
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a target="_blank" rel="noopener noreferrer">
              设置
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a target="_blank" rel="noopener noreferrer">
              保存草稿
            </a>
          ),
        },
        {
          key: '4',
          label: (
            <a target="_blank" rel="noopener noreferrer">
              删除
            </a>
          ),
        },
      ]}
    />
  );

  const confirm = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    history.push('/');
  };

  return (
    <div>
      {/* 顶部 */}
      <Card className={styles.cardEditor}>
        <div className={styles.editorCard}>
          <div className={styles.editorLeft}>
            <Popconfirm
              title="Are you sure to delete this task?"
              onConfirm={confirm}
              okText="Yes"
              cancelText="No"
            >
              <Button className={styles.button}>X</Button>
            </Popconfirm>
            <span className="ant-page-header-heading-title">
              <input
                type="text"
                className={styles.antinput}
                placeholder="请输入文章标题"
              />
            </span>
          </div>
          <div className={styles.editorRight}>
            <Button type="primary">
              <span>发 布</span>
            </Button>
            <Dropdown overlay={menu} placement="bottomLeft">
              <span role="img" aria-label="ellipsis">
                <svg
                  viewBox="64 64 896 896"
                  focusable="false"
                  data-icon="ellipsis"
                  width="1em"
                  height="1em"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path d="M176 511a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0zm280 0a56 56 0 10112 0 56 56 0 10-112 0z"></path>
                </svg>
              </span>
            </Dropdown>
          </div>
        </div>
      </Card>
      <div className={styles.center}>
        <iframe
          src="https://jasonandjay.com/editor/static/"
          width={'100%'}
          height={700}
        ></iframe>
      </div>
    </div>
  );
}
